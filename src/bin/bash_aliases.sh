alias tserv=torchserve
alias tserve=torchserve
alias tsrv=torchserve
alias ts=torchserve  # this overrides the bash util ts which adds timestamps to each line of input
alias lo=libreoffice
alias sp=python manage.py shell_plus
alias gpaste=gpaste-ui
